package br.com.itau;

public class Main {
    public static void main(String[] args) {

        Pessoa pessoa = new Pessoa("pessoa1", "123", 25, 0, 0);
        System.out.println(pessoa.getSomaPrivilegio());
        System.out.println(pessoa.atribuirPrivilegioMaster());

        Funcionario funcionario = new Funcionario("func1", "456", 30, 0, 0, 00456);
        System.out.println(funcionario.getSomaPrivilegio());
        System.out.println(funcionario.atribuirPrivilegioMaster());

    }
}
