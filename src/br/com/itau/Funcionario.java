package br.com.itau;

public class Funcionario extends Pessoa{
    protected int racf;

    public Funcionario(String nome, String cpf, int idade, int privilegio, int privilegioMaster, int racf) {
        super(nome, cpf, idade, privilegio, privilegioMaster);
        this.racf = racf;
    }

    @Override
    public int getSomaPrivilegio() {
        return privilegio = 2;
    }
    
    @Override
    public int atribuirPrivilegioMaster() {
        return super.atribuirPrivilegioMaster();
    }

}
