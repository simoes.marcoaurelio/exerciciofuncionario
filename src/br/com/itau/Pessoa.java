package br.com.itau;

public class Pessoa {

    protected String nome;
    protected String cpf;
    protected int idade;
    protected int privilegio;
    private int privilegioMaster;

    public Pessoa(String nome, String cpf, int idade, int privilegio, int privilegioMaster) {
        this.nome = nome;
        this.cpf = cpf;
        this.idade = idade;
        this.privilegio = privilegio;
        this.privilegioMaster = privilegioMaster;
    }

    public int getSomaPrivilegio() {
        return privilegio = 1;
    }

    public int atribuirPrivilegioMaster() {
        return privilegioMaster += privilegio + 10;
    }
}
